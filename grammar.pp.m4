changequote({,})
define({_GENRE_}, patsubst(esyscmd(mysql -N<<<"SET @@group_concat_max_len=1024*20; SELECT GROUP_CONCAT(DISTINCT genre ORDER BY genre SEPARATOR '|') FROM bdtfx WHERE genre != ''"), +, \\+))dnl
define({_NOM_SUPRA_GENERIQUE_}, esyscmd(mysql -N<<<"SET @@group_concat_max_len=1024*1024; SELECT GROUP_CONCAT(DISTINCT nom_supra_generique ORDER BY nom_supra_generique SEPARATOR '|') FROM bdtfx WHERE nom_supra_generique != ''"))
define({_CULTIVAR_GROUPE_}, esyscmd(mysql -N<<<"SET @@group_concat_max_len=1024*1024; SELECT GROUP_CONCAT(DISTINCT cultivar_groupe ORDER BY cultivar_groupe SEPARATOR '|') FROM bdtfx WHERE cultivar_groupe != ''"))
define({_CULTIVAR_}, esyscmd(mysql -N<<<"SET @@group_concat_max_len=1024*1024; SELECT GROUP_CONCAT(DISTINCT cultivar ORDER BY cultivar SEPARATOR '|') FROM bdtfx WHERE cultivar != ''"))

%token space \s

%token nom_supra_generique _NOM_SUPRA_GENERIQUE_
%token genre _GENRE_
%token type_epithete (f\.)|proles|sublusus|(subsp\.)|(subvar\.)|(var\.)

%token cultivar_groupe _CULTIVAR_GROUPE_
%token cultivar _CULTIVAR_

%token epithete_infra_sp_chars [a-zëï-]+
%token chimere x \s [a-zëï-]+
%token hybride \+ \s [a-zëï-]+

%token epithete_sp_chars sp\.[A-Z0-9]

%token auteur [A-Za-z &'(),-.ÁÅÉÓÖØÜáâäåæçèéëíñòóôöøúüýćČčĕěğłńňŠšž"]+


#nom_sci:
        ( <nom_supra_generique> | <genre> | biggenre() ) ( ::space:: <auteur> ) ? ::space::*
#biggenre:
        genrecul() | genreepi() | genreepiinf()
genrecul:
        <genre> ::space:: cul()
genreepi:
        <genre> ::space:: epi()
genreepiinf:
        <genre> ::space:: epiinf()
epi:
        epithete_sp() ( ::space:: cul() ) ?
cul:
        <cultivar_groupe> | <cultivar>
epiinf:
        epithete_sp() ::space:: <type_epithete> ::space:: epithete_infra_sp() ( ::space:: cul() ) ?
#epithete_infra_sp:
        <epithete_infra_sp_chars> | <chimere> | <hybride> | ( genrecul() | genreepi() )
#epithete_sp:
        <chimere> | <hybride> | <epithete_sp_chars>
