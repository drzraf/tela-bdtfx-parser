aide:
	echo nada

build:
	m4 < grammar.pp.m4 >| grammar.pp

test:
# limit de taille des regexp
	echo a|./hoa compiler:pp  --visitor dump grammar.pp 0
# doit matcher
	echo Acanthaceae|./hoa compiler:pp  --visitor dump grammar.pp 0
# aussi
	echo Acer|./hoa compiler:pp  --visitor dump grammar.pp 0
#	echo test|php -d pcre.backtrack_limit=23001337 -d pcre.recursion_limit=23001337 hoa compiler:pp  --visitor dump grammar.pp 0

prepare:
	git clone https://github.com/hoaproject/Core.git Hoa/Core
	git clone http://git.hoa-project.net/Library/Compiler.git Hoa/Compiler
	git clone http://git.hoa-project.net/Library/Console.git Hoa/Console
	git clone http://git.hoa-project.net/Library/Routeur.git Hoa/Routeur
	git clone http://git.hoa-project.net/Library/Router.git Hoa/Routeur
	git clone http://git.hoa-project.net/Library/Router.git Hoa/Router
	git clone http://git.hoa-project.net/Library/Dispatcher.git Hoa/Dispatcher
	git clone http://git.hoa-project.net/Library/Stream.git Hoa/Stream
	git clone http://git.hoa-project.net/Library/File.git Hoa/File
	git clone http://git.hoa-project.net/Library/Visitor.git Hoa/Visitor
