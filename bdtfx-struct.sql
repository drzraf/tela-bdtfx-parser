DROP TABLE IF EXISTS `bdtfx`;
CREATE TABLE IF NOT EXISTS `bdtfx` (
num_nom	INT(9),
num_nom_retenu	VARCHAR(9),
num_tax_sup	VARCHAR(9),
rang	INT(4),
nom_sci VARCHAR(500),
nom_supra_generique VARCHAR(100),
genre VARCHAR(100),
epithete_infra_generique VARCHAR(100),
epithete_sp VARCHAR(100),
type_epithete VARCHAR(100),
epithete_infra_sp VARCHAR(100),
cultivar_groupe VARCHAR(100),
cultivar VARCHAR(100),
nom_commercial VARCHAR(100),
auteur VARCHAR(100),
annee VARCHAR(4),
biblio_origine VARCHAR(500),
notes TEXT,
nom_addendum VARCHAR(500),
homonyme VARCHAR(1),
num_type VARCHAR(9),
num_basionyme VARCHAR(9),
synonyme_proparte VARCHAR(100),
synonyme_douteux VARCHAR(1),
synonyme_mal_applique VARCHAR(1),
synonyme_orthographique VARCHAR(9),
orthographe_originelle VARCHAR(100),
hybride_parent_01 VARCHAR(9),
hybride_parent_01_notes TEXT,
hybride_parent_02 VARCHAR(9),
hybride_parent_02_notes TEXT,
nom_francais VARCHAR(500),
presence	VARCHAR(100),
statut_origine	VARCHAR(100),
statut_introduction	VARCHAR(100),
statut_culture	VARCHAR(100),
exclure_taxref INT (1),

presence_Ga INT(1),
presence_Co INT(1),
num_taxonomique INT(9),
PRIMARY KEY (`num_nom`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

